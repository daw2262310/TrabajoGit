### Lista de cosas por hacer
-Hay fallos de redacción en todas las ramas, esos serán arreglados con una rama hotfix una vez esté toda la funcionalidad terminada, es decir, una vez no vaya a haber más cambios ni adiciones a ninguna de las ramas, o, al menos, en la sección que vayamos a abrir la rama hotfix.  
-Faltan ejemplos en la rama de integración continua.  
-GIT: Faltan las ventajas, comandos avanzados(cherry pick), distintos tipos de clientes.  
-IC: Falta como funcionan los runners y los pipelines, cómo se desplegaría y la explicación del .yml asociado.  
-Falta realizar lo de Hugo, que debe hacerse al terminar todo el proyecto, con todos los merges hechos.(No nos va a dar tiempo..xD)